package java_imc;

import java.util.Scanner;
import java_imc.Enumeration.Unit_Size;
import java_imc.Enumeration.Unit_Weight;

/**
 *
 * @author richardfl
 */
public class IMC {

    private static double poids;
    private static double taille;
    private static double imc;

    public static void imc() {

        String str;
        Scanner sc = new Scanner(System.in);

        System.out.println("Voulez-vous calculer votre IMC ? (O/N)");
        str = sc.nextLine();
        if ("O".equals(str.toUpperCase())) {
            poids = askDouble(sc, "poids", "kg");
            taille = askDouble(sc, "taille", "m");
            calcul();
        } else if ("N".equals(str.toUpperCase())) {
            System.out.println("Erreur, arret du programme...");
            System.exit(5);
        } else {
            System.out.println("Erreur, arret du programme...");
            System.exit(5);
        }
    }

    private static double askDouble(Scanner sc, String type, String unit) {

        System.out.println("Entrez votre " + type + " ( en " + unit + " ) : ");
        String valeur = sc.nextLine();
        valeur = valeur.replace(",", ".");
        double doubleTake = Double.parseDouble(valeur);
        System.out.println("Votre " + type + " est bien en " + unit + " ? (O/N)");
        valeur = sc.nextLine();
        
        if ("O".equals(valeur.toUpperCase())) {
        }
         else if ("N".equals(valeur.toUpperCase())) {
            String result = getUnit(sc, type);     
        }
        return doubleTake;
    }

    public static String getUnit(Scanner sc, String type){
        String str = null;
        String ask = "Veuillez saisir l'unité dans laquelle vous avez saisi votre " + type + "." + "Vous avez le choix entre les valeurs suivantes : ";
        System.out.println(ask);
        switch (type.toLowerCase()){
            case "taille" : 
                System.out.println(Unit_Size.toStringUnits());
        String unit = sc.nextLine();
            break;
            case "poids" : 
                System.out.println(Unit_Weight.toStringUnits());
                unit = sc.nextLine();   
        }
        return null;
    }
    public static void calcul() {
        //Calcule de l'IMC en fonction du poids et de la taille de la personne//
        imc = poids / (taille * taille);

        //Calcule pour arrondir le résultat//
        imc = (double) Math.round(imc * 10) / 10;

        //Choix d'une phrase en fonction du resultat récupéré//
        System.out.println(
                "Votre IMC est de " + imc + " pour un poids de " + poids + " kg et pour une taille de " + taille + " m");

        if (imc < 16.5) {
            System.out.println("Votre IMC correspond à une dénutrion / anorexie.");
        } else if (imc >= 16.5 && imc < 18.5) {
            System.out.println("Votre IMC correspond à une maigreur.");
        } else if (imc >= 18.5 && imc < 25) {
            System.out.println("Votre IMC correspond à une corpulence normale.");
        } else if (imc >= 25 && imc < 30) {
            System.out.println("Votre IMC correspond à un surpoids.");
        } else if (imc >= 30 && imc < 35) {
            System.out.println("Votre IMC correspond à une obsité modérée.");
        } else if (imc >= 35 && imc < 40) {
            System.out.println("Votre IMC correspond à une obésité sévère.");
        } else {
            System.out.println("Votre IMC correspond à une obésité morbide ou massive.");
        }

    }
}
