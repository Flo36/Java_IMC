/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_imc.Enumeration;

/**
 *
 * @author frichard
 */
public enum Unit_Size {

    M("m", 1), CM("cm", 0.01), IN("inch", 2.54);
    private static int length;

    private final double coefToM;
    private final String unit;

    Unit_Size(String _unit, double _coefToM) {
        this.unit = _unit;
        this.coefToM = _coefToM;
    }

    public static int getLength() {
        return length;
    }

    public double getCoefToM() {
        return coefToM;
    }

    public String getUnit() {
        return unit;
    }

    public static Unit_Size getSizeFromUnit(String _unit) {
        for (Unit_Size c : values()) {
            if (c.unit.toUpperCase().equals(_unit.toUpperCase())) {
                return c;
            }
        }
        return null;
    }

    public static String toStringUnits() {
        String res = "";
        Unit_Size[] tabUs = values();
        for (int c = 0; c < tabUs.length; c++) {
            Unit_Size us = tabUs[c];
            if (c == tabUs.length -1) {
                res += us.getUnit();
            } else {
                res += us.getUnit() + ", ";
            }
        }

        return res;
    }
}
