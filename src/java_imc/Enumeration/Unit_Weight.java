/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_imc.Enumeration;

import static java_imc.Enumeration.Unit_Size.values;

/**
 *
 * @author frichard
 */
public enum Unit_Weight {

    KG("kg", 1), G("g", 0.01), LB("lb", 0.4536);

    private final double coefToKg;
    private final String unit;

    Unit_Weight(String _unit, double _coefToKg) {
        this.unit = _unit;
        this.coefToKg = _coefToKg;
    }

    public double getCoefToKg() {
        return coefToKg;
    }

    public String getUnit() {
        return unit;
    }
     
    public static Unit_Weight getWeightFromUnit(String _unit) {
        for (Unit_Weight c : values()) {
            if (c.unit.toUpperCase().equals(_unit.toUpperCase())) {
                return c;
            }
        }
        throw new IllegalArgumentException(_unit);
    }
    
    public static String toStringUnits() {
        String res = "";
        Unit_Weight[] tabUs = values();
        for (int c = 0; c < tabUs.length; c++) {
            Unit_Weight us = tabUs[c];
            if (c == tabUs.length -1) {
                res += us.getUnit();
            } else {
                res += us.getUnit() + ", ";
            }
        }

        return res;
    }
}
